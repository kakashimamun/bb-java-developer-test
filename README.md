# BB Java Developer Test

> Original task link `https://github.com/BookingBoss/java-test`

The project is a super fast Java powered web service to:

- Store products from a web request
- Retrieve products from a web request


## API Details

- Products : REST API for Products
- GET /products Get all product IDs
- GET /products/pageable Get paginated product IDs
- POST /products Post list of product value objects to insert
- DELETE /products/{productId} delete a product
- GET /products/{productId} Get a product details
- PATCH /products/{productId} Partial update product
- PUT /products/{productId} Update a product


### Pre-requisites
You need to have:
- Java 8+
- Maven 3+

### How to run tests
- run `mvn test` in root folder
### How to build
- run `mvn clean install` in root folder
###How to run
- run `mvn spring-boot:run`

### Access to APIs
- Point your local browser to: `http://localhost:8080/api/v1/swagger-ui.html`
or you can access API endpoints directly

### Things to remember
H2DBInitConfig.class contains database init sql. It will be applied every time server starts. 



