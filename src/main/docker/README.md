# BB Java Developer Test

> This is a simple test to show us how awesome(!) I am

The project is a super fast Java powered web service to:

- Store products from a web request
- Retrieve products from a web request


## API Details

- Products : REST API for Products
- GET /products Get all product IDs
- GET /products/pageable Get paginated product IDs
- POST /products Post list of product value objects to insert
- DELETE /products/{productId} delete a product
- GET /products/{productId} Get a product details
- PATCH /products/{productId} Partial update product
- PUT /products/{productId} Update a product

### Pre-requisites
You need to have:
- a Docker installation available locally. See [Docker website](https://docs.docker.com/install/) on installation instructions for Windows and Linux users.
- at least 100MB of local storage.
- springboot-jwt project sources locally compiled, and shell pointing to project root.

### Build image
`mvn clean package docker:build`.

### Start container

#### From Maven, default endpoint
Basic usage with defaults: server IP 127.0.0.1, server port 9081

`mvn docker:start`

#### From Maven, custom endpoint
Advanced usage: custom server IP and server port

`mvn docker:start "-Dtomcat.ip=<SERVER_IP>"  "-Dtomcat.port=<SERVER_PORT>"`

where `<SERVER_IP>` and `<SERVER_PORT>` are the IP address and port where Maven will test health check against (usually localhost, 127.0.0.1, or 192.168.99.100 on legacy Docker Toolbox).

#### From command line
`docker run -d -p <SERVER_PORT>:8080 springboot-jwt`

Now if you issue a `docker ps` command you should see a new running container listed.

To see logs:
`docker logs -f <CONTAINERID>`

### Access to APIs
Point your local browser to:



