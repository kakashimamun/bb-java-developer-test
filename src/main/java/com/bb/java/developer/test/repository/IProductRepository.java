package com.bb.java.developer.test.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import com.bb.java.developer.test.models.Product;
import org.springframework.data.repository.PagingAndSortingRepository;
import java.util.List;

public interface IProductRepository extends PagingAndSortingRepository<Product, Long> {

    @Query(value = "SELECT p.id FROM Product p")
    List<Long> getAllId();

    @Query(value = "SELECT p.id FROM Product p")
    List<Long> getAllId(Pageable pageable);
}