package com.bb.java.developer.test.service;


import com.bb.java.developer.test.models.Product;
import com.bb.java.developer.test.entities.ProductIdWrapper;
import com.bb.java.developer.test.entities.ProductVO;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface IProductService {

    List<Product> getProducts();

    Product getProductById(Long productId);

    List<ProductIdWrapper> getAllProductsIds();

    List<ProductIdWrapper> getProductsIds(Pageable pageable);

    Product update(long productId, ProductVO valueObject);

    ProductIdWrapper delete(long productId);

    List<Product> bulkInsert(List<ProductVO> vos);

    Product partialUpdate(long productId, Map<String, Object> updates);

}
