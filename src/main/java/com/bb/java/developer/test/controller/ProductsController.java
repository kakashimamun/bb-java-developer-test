package com.bb.java.developer.test.controller;


import com.bb.java.developer.test.models.Product;
import com.bb.java.developer.test.entities.ApiResponseEntity;
import com.bb.java.developer.test.entities.ProductIdWrapper;
import com.bb.java.developer.test.entities.ProductVO;
import com.bb.java.developer.test.service.IProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Product Controller
 */
@RestController
@RequestMapping(path = "/products")
@Api(value = "Products", description = "REST API for Products", tags = {"Products"})
@Validated
public class ProductsController {
    private IProductService productService;

    @Autowired
    public ProductsController(IProductService productService) {
        this.productService = productService;
    }

    @ApiOperation(value = "Get all product IDs")
    @RequestMapping(path = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getIds() {
        List<ProductIdWrapper> ids = productService.getAllProductsIds();
        return ResponseEntity.ok().body(new ApiResponseEntity<>(ids));
    }

    @ApiOperation(value = "Get paginated product IDs")
    @RequestMapping(path = "/pageable", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                    value = "Results page you want to retrieve (0..N)"),
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                    value = "Number of records per page."),
    })
    public ResponseEntity getIds(Pageable pageable) {
        List<ProductIdWrapper> ids = productService.getProductsIds(pageable);
        return ResponseEntity.ok().body(new ApiResponseEntity<>(ids));
    }

    @ApiOperation(value = "Get a product details")
    @RequestMapping(path = "/{productId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getOne(@PathVariable @Min(1) long productId) {
        Product products;
        products = productService.getProductById(productId);
        return ResponseEntity.ok().body(new ApiResponseEntity<>(Collections.singletonList(products)));
    }

    @ApiOperation(value = "Post list of product value objects to insert")
    @RequestMapping(path = "", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity post(@RequestBody List<ProductVO> vos) {
        List<Product> products = productService.bulkInsert(vos);
        return ResponseEntity.ok().body(new ApiResponseEntity<>(products));
    }

    @ApiOperation(value = "Update a product")
    @RequestMapping(path = "/{productId}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity put(@PathVariable @Min(1) long productId,@Valid @RequestBody ProductVO valueObject) {
        Product product = productService.update(productId, valueObject);
        return ResponseEntity.ok().body(new ApiResponseEntity<>(Collections.singletonList(product)));
    }

    @ApiOperation(value = "Partial update product")
    @RequestMapping(path = "/{productId}", method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity patch(@PathVariable @Min(1) long productId, @RequestBody Map<String, Object> updates) {
        Product product = productService.partialUpdate(productId, updates);
        return ResponseEntity.ok().body(new ApiResponseEntity<>(Collections.singletonList(product)));
    }

    @ApiOperation(value = "delete a product")
    @RequestMapping(path = "/{productId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity delete(@PathVariable @Min(1) long productId) {
        ProductIdWrapper id = productService.delete(productId);
        return ResponseEntity.ok().body(new ApiResponseEntity<>(Collections.singletonList(id)));
    }
}
