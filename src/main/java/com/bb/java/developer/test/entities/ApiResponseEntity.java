package com.bb.java.developer.test.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.slf4j.MDC;

import static com.bb.java.developer.test.entities.MDCKeys.DEFAULT_EVENT_TIMESTAMP;
import static com.bb.java.developer.test.entities.MDCKeys.DEFAULT_EVENT_UUID_KEY;


@Data
@NoArgsConstructor
public class ApiResponseEntity<T> {

    private String id;
    private String timestamp;
    private T products;

    public ApiResponseEntity(T products){
        this.products = products;
        this.id = MDC.get(DEFAULT_EVENT_UUID_KEY);
        this.timestamp = MDC.get(DEFAULT_EVENT_TIMESTAMP);
    }
}
