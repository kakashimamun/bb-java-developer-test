
package com.bb.java.developer.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
public class BBJDTestApplication {
    	public static void main(String[] args) {
			ApplicationContext ctx = SpringApplication.run(BBJDTestApplication.class, args);
    }

}
