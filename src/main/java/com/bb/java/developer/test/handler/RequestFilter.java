package com.bb.java.developer.test.handler;

import com.bb.java.developer.test.entities.MDCKeys;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

import static com.bb.java.developer.test.entities.MDCKeys.DEFAULT_EVENT_TIMESTAMP;
import static com.bb.java.developer.test.entities.MDCKeys.DEFAULT_EVENT_UUID_KEY;

/**
 * Filter to insert UUID and time into @{@link MDC}
 */
@Component
public class RequestFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response, final FilterChain chain)
            throws java.io.IOException, ServletException {
        String uuidKey = MDCKeys.DEFAULT_EVENT_UUID_KEY;
        String timestampKey = MDCKeys.DEFAULT_EVENT_TIMESTAMP;
        try {
            final String token =  UUID.randomUUID().toString().toUpperCase().replace("-", "");
            MDC.put(uuidKey, token);
            MDC.put(timestampKey, String.valueOf(System.currentTimeMillis()));
            chain.doFilter(request, response);
        } finally {
            MDC.remove(uuidKey);
            MDC.remove(timestampKey);
        }
    }
}