package com.bb.java.developer.test.service.impl;

import com.bb.java.developer.test.repository.IProductRepository;
import com.bb.java.developer.test.error.handling.exception.ItemNotFoundException;
import com.bb.java.developer.test.models.Product;
import com.bb.java.developer.test.entities.ProductIdWrapper;
import com.bb.java.developer.test.entities.ProductVO;
import com.bb.java.developer.test.factory.ProductFacotry;
import com.bb.java.developer.test.util.ProductUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.bb.java.developer.test.service.IProductService;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ProductService implements IProductService {

	private IProductRepository productDao;

	private Validator validator;

	@Autowired
	public ProductService(IProductRepository productDao, Validator validator){
		this.productDao = productDao;
		this.validator = validator;
	}

	@Override
	public List<Product> getProducts() {
		ArrayList<Product> products = new ArrayList<>();
		productDao.findAll().forEach(products::add);
		return products;
	}

	@Override
	public Product getProductById(Long productId) {
		Product product = productDao.findOne(productId);
		if(product==null) throw new ItemNotFoundException(String.format("Product with %d doesn't exists.",productId));
		else return product;
	}

	@Override
	public List<ProductIdWrapper> getAllProductsIds() {

		ArrayList<Long> ids = new ArrayList<>(productDao.getAllId());
		return ids
				.stream()
				.map(p->ProductIdWrapper.builder().id(p).build())
				.collect(Collectors.toList());
	}

	@Override
	public List<ProductIdWrapper> getProductsIds(Pageable pageable) {

		ArrayList<Long> ids = new ArrayList<>(productDao.getAllId(pageable));
		return ids
				.stream()
				.map(p->ProductIdWrapper.builder().id(p).build())
				.collect(Collectors.toList());
	}

	@Override
	public Product update(long productId, ProductVO valueObject) {
		Product product = this.getProductById(productId);
		Product updatedProduct = ProductFacotry.getProduct(valueObject);
		updatedProduct.setId(product.getId());
		product = productDao.save(updatedProduct);
		return product;
	}

    @Override
    public ProductIdWrapper delete(long productId) {
        productDao.delete(productId);
        return ProductIdWrapper.builder().id(productId).build();
    }

	@Override
	public List<Product> bulkInsert(List<ProductVO> vos) {
		List<Product> products = vos.stream().map(ProductFacotry::getProduct).collect(Collectors.toList());
		List<Product> savedList = new ArrayList<>();
		productDao.save(products).forEach(savedList::add);
		return savedList;
	}

	@Override
	public Product partialUpdate(long productId, Map<String, Object> updates) {
		Product product = this.getProductById(productId);

		product = ProductUpdater.applyUpdate(product,updates);

		Set<ConstraintViolation<Product>> violations = validator.validate(product);

		if(violations.size()>0)
			throw new ConstraintViolationException(violations);
		return productDao.save(product);
	}
}
