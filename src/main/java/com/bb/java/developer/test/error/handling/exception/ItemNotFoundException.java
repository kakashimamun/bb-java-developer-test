package com.bb.java.developer.test.error.handling.exception;

public class ItemNotFoundException  extends RuntimeException{
    public ItemNotFoundException(String message) {
        super(message);
    }
}
