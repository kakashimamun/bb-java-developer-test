package com.bb.java.developer.test.entities;

public final class MDCKeys {
    public static final String DEFAULT_EVENT_UUID_KEY = "EVENT_ID";
    public static final String DEFAULT_EVENT_TIMESTAMP = "EVENT_TIME";
}
