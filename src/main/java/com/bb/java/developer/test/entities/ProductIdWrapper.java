package com.bb.java.developer.test.entities;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * Wrapper on id for api response
 */
@Data
@EqualsAndHashCode
@Builder
public class ProductIdWrapper {
    long id;
}
