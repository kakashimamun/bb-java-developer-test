package com.bb.java.developer.test.error.handling.exception;

public class NoPartialUpdateException extends RuntimeException {
    public NoPartialUpdateException(String msg) {
        super(msg);
    }
}
