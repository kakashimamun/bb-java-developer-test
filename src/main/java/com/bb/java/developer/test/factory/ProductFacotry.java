package com.bb.java.developer.test.factory;

import com.bb.java.developer.test.models.Product;
import com.bb.java.developer.test.entities.ProductVO;

/**
 * Product factory brean
 */
public abstract class ProductFacotry {

    public static Product getProduct(){
        return new Product();
    }

    public static Product getProduct(ProductVO productVO){
        Product product = getProduct();
        product.setName(productVO.getName());
        product.setQuantity(productVO.getQuantity());
        product.setSaleAmount(productVO.getSaleAmount());

        return product;
    }


}
