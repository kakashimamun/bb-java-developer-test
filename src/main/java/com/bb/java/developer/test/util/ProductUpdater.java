package com.bb.java.developer.test.util;

import com.bb.java.developer.test.error.handling.exception.NoPartialUpdateException;
import com.bb.java.developer.test.models.Product;
import com.google.common.base.CaseFormat;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.SerializationUtils;

import java.lang.reflect.Field;
import java.util.Map;

@Slf4j
public abstract class ProductUpdater {
    public static Product applyUpdate(Product product, Map<String, Object> updates) {

        Product updatedProduct = (Product) SerializationUtils.deserialize(SerializationUtils.serialize(product));

        for(Map.Entry<String,Object> entry:updates.entrySet()){
            try {
                setField(updatedProduct,
                        CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL,entry.getKey()),
                        entry.getValue());
            } catch (Exception e) {
                log.info("Couldn't set value for field {}",entry.getKey());
            }
        }
        if(updatedProduct.equals(product)) {
            log.warn("partial updates weren't applied");
            throw new NoPartialUpdateException("No suitable updates found");
        }
        return updatedProduct;
    }

    /**
     * Returns the first {@link Field}
     */
    private static Field getField(Class<?> clazz, String name) {
        Field field = null;
        while (clazz != null && field == null) {
            try {
                field = clazz.getDeclaredField(name);
            } catch (Exception e) {
                log.warn("Exception in finding field {}:{}",clazz,name);
            }
            clazz = clazz.getSuperclass();
        }
        return field;
    }

     /**
      * Sets {@code value} to the first {@link Field} in the {@code object} hierarchy
      */
    private static void setField(Object object, String fieldName, Object value) throws Exception {
        Field field = getField(object.getClass(), fieldName);
        field.setAccessible(true);
        field.set(object, value);
    }
}
