package com.bb.java.developer.test.error.handling;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import org.slf4j.MDC;
import org.springframework.http.HttpStatus;

import java.util.Optional;

import static com.bb.java.developer.test.entities.MDCKeys.DEFAULT_EVENT_TIMESTAMP;
import static com.bb.java.developer.test.entities.MDCKeys.DEFAULT_EVENT_UUID_KEY;

/**
 * Standard Api error response class
 */
@Data
public class ApiErrorResponse {

    private static final String DEFAULT_MSG = "Unexpected error";
    private static final String DEFAULT_DBG_MSG = "Unknown server error. check logs";

    private String id;
    private String timestamp;
    private HttpStatus status;

    @Builder
    @Getter
    private static class ErrorDetails{
        private String message;
        private String debugMessage;
    }

    ErrorDetails error;

    private ApiErrorResponse() {
        this.id = MDC.get(DEFAULT_EVENT_UUID_KEY);
        this.timestamp = MDC.get(DEFAULT_EVENT_TIMESTAMP);

    }

    ApiErrorResponse(HttpStatus status) {
        this();
        this.status = status;
        this.error = ErrorDetails.builder()
                .message(DEFAULT_MSG)
                .debugMessage(DEFAULT_DBG_MSG)
                .build();
    }

    ApiErrorResponse(HttpStatus status, Throwable ex) {
        this(status);
        this.error = ErrorDetails.builder()
                .message(DEFAULT_MSG)
                .debugMessage(Optional.ofNullable(ex.getLocalizedMessage()).orElse(DEFAULT_DBG_MSG))
                .build();
    }

    ApiErrorResponse(HttpStatus status, String message, Throwable ex) {
        this(status);
        this.error = ErrorDetails.builder()
                .message(message)
                .debugMessage(Optional.ofNullable(ex.getLocalizedMessage()).orElse(DEFAULT_DBG_MSG))
                .build();
    }
}