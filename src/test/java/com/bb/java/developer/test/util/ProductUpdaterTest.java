package com.bb.java.developer.test.util;

import com.bb.java.developer.test.error.handling.exception.NoPartialUpdateException;
import com.bb.java.developer.test.factory.ProductFacotry;
import com.bb.java.developer.test.models.Product;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;

@RunWith(SpringRunner.class)
public class ProductUpdaterTest {

    Product product;

    @Before
    public void setUp(){
        product = ProductFacotry.getProduct();

        product.setId(1);
        product.setName("Dummy");
        product.setQuantity(100);
        product.setSaleAmount(10);
    }
    @Test
    public void applyUpdateValid() {

        HashMap<String,Object> map = new HashMap<>();
        map.put("name","updatedName");
        map.put("quantity",200);
        map.put("sale_amount",20);

        Product updated = ProductUpdater.applyUpdate(product,map);

        Assert.assertEquals(updated.getId(),1);
        Assert.assertEquals(updated.getName(),"updatedName");
        Assert.assertEquals(updated.getQuantity(),200);
        Assert.assertEquals(updated.getSaleAmount(),20);

    }

    @Test(expected = NoPartialUpdateException.class)
    public void applyUpdateInvalid() {

        HashMap<String,Object> map = new HashMap<>();
        map.put("nameD","updatedName");
        map.put("quantityd",200);
        map.put("sale_amountd",20);

        Product updated = ProductUpdater.applyUpdate(product,map);
    }
}