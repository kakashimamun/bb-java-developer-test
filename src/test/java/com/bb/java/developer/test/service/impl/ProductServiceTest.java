package com.bb.java.developer.test.service.impl;

import com.bb.java.developer.test.BBJDTestApplication;
import com.bb.java.developer.test.config.H2TestConfig;
import com.bb.java.developer.test.repository.IProductRepository;
import com.bb.java.developer.test.entities.ProductIdWrapper;
import com.bb.java.developer.test.entities.ProductVO;
import com.bb.java.developer.test.error.handling.exception.ItemNotFoundException;
import com.bb.java.developer.test.error.handling.exception.NoPartialUpdateException;
import com.bb.java.developer.test.models.Product;
import com.bb.java.developer.test.service.IProductService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.List;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {H2TestConfig.class, BBJDTestApplication.class})
public class ProductServiceTest {

    @Autowired
    IProductService productService;

    @Autowired
    IProductRepository productDao;

    @Test
    public void getProducts() throws Exception {
        List<Product> products = productService.getProducts();

        Assert.assertNotNull(products);
        Assert.assertFalse(products.isEmpty());
    }

    @Test
    public void getProductById() throws Exception {

        long id = 1;
        Product product = productService.getProductById(id);

        Assert.assertNotNull(product);
        Assert.assertEquals(product.getId(), id);
    }

    @Test
    public void getAllProductsIds() throws Exception {

        List<ProductIdWrapper> id = productService.getAllProductsIds();

        Assert.assertNotNull(id);
        Assert.assertFalse(id.isEmpty());
    }

    @Test
    public void getProductsIds() throws Exception {

        Pageable p = new PageRequest(0, 10);

        List<ProductIdWrapper> products = productService.getProductsIds(p);

        Assert.assertNotNull(products);
        Assert.assertFalse(products.isEmpty());
        Assert.assertEquals(products.size(), p.getPageSize());
    }

    @Test
    public void update() throws Exception {

        long id = 12;
        ProductVO vo = new ProductVO();
        vo.setName("DummyUpdate");
        vo.setQuantity(1000);
        vo.setSaleAmount(100);

        Product product = productService.update(id, vo);

        Assert.assertNotNull(product);
        Assert.assertEquals(product.getId(), id);
        Assert.assertEquals(product.getName(), "DummyUpdate");
        Assert.assertEquals(product.getQuantity(), 1000);
        Assert.assertEquals(product.getSaleAmount(), 100);

    }

    @Test(expected = ItemNotFoundException.class)
    public void delete() throws Exception {

        long id = 15;

        ProductIdWrapper idWrapper = productService.delete(id);

        productService.getProductById(idWrapper.getId());
    }


    @Test
    public void partialUpdate() throws Exception {

        long id = 10;

        HashMap<String, Object> map = new HashMap<>();
        map.put("quantity", 100);
        map.put("sale_amount", 10);

        Product product = productService.partialUpdate(id, map);

        Assert.assertNotNull(product);
        Assert.assertEquals(product.getId(), id);
        Assert.assertEquals(product.getQuantity(), 100);
        Assert.assertEquals(product.getSaleAmount(), 10);

    }

    @Test(expected = ConstraintViolationException.class)
    public void partialUpdateFail() throws Exception {

        long id = 13;

        HashMap<String, Object> map = new HashMap<>();
        map.put("quantity", -100);
        map.put("sale_amount", 10);

        Product product = productService.partialUpdate(id, map);

        Assert.assertNotNull(product);
        Assert.assertEquals(product.getId(), id);
        Assert.assertEquals(product.getQuantity(), 100);
        Assert.assertEquals(product.getSaleAmount(), 10);

    }

    @Test(expected = NoPartialUpdateException.class)
    public void partialUpdateNoUpdate() throws Exception {

        long id = 13;

        HashMap<String, Object> map = new HashMap<>();

        Product product = productService.partialUpdate(id, map);

        Assert.assertNotNull(product);
        Assert.assertEquals(product.getId(), id);
        Assert.assertEquals(product.getQuantity(), 100);
        Assert.assertEquals(product.getSaleAmount(), 10);

    }

}