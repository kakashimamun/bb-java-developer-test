package com.bb.java.developer.test.validation;

import com.bb.java.developer.test.factory.ProductFacotry;
import com.bb.java.developer.test.models.Product;
import org.hibernate.validator.HibernateValidator;
import org.junit.Before;

import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
public class ProductValidationTest {

    private static Validator validator;

    private static LocalValidatorFactoryBean localValidatorFactory;

    @Before
    public void setup() {
        localValidatorFactory = new LocalValidatorFactoryBean();
        localValidatorFactory.setProviderClass(HibernateValidator.class);
        localValidatorFactory.afterPropertiesSet();

        validator = localValidatorFactory.getValidator();
    }

    @AfterClass
    public static void close() {
        localValidatorFactory.close();
    }

    @Test
    public void shouldHaveNoViolations() {
        //given:
        Product product = ProductFacotry.getProduct();

        product.setId(1);
        product.setName("Dummy");
        product.setQuantity(100);
        product.setSaleAmount(10);

        //when:
        Set<ConstraintViolation<Product>> violations
                = validator.validate(product);

        //then:
        assertTrue(violations.isEmpty());
    }

    @Test
    public void shouldDetectInvalidName() {
        //given too short name:
        Product product = ProductFacotry.getProduct();

        product.setId(1);
        product.setName("");
        product.setQuantity(100);
        product.setSaleAmount(10);

        //when:
        Set<ConstraintViolation<Product>> violations
                = validator.validate(product);

        //then:
        assertEquals(violations.size(), 1);

        ConstraintViolation<Product> violation
                = violations.iterator().next();
        assertEquals("may not be empty",
                violation.getMessage());
        assertEquals("name", violation.getPropertyPath().toString());
    }

    @Test
    public void shouldDetectInvalidQuantity() {
        //given too short name:
        Product product = ProductFacotry.getProduct();

        product.setId(1);
        product.setName("Dummy");
        product.setQuantity(-100);
        product.setSaleAmount(10);

        //when:
        Set<ConstraintViolation<Product>> violations
                = validator.validate(product);

        //then:
        assertEquals(violations.size(), 1);

        ConstraintViolation<Product> violation
                = violations.iterator().next();
        assertEquals("must be greater than or equal to 0",
                violation.getMessage());
        assertEquals("quantity", violation.getPropertyPath().toString());
    }

    @Test
    public void shouldDetectInvalidSaleAmount() {
        //given too short name:
        Product product = ProductFacotry.getProduct();

        product.setId(1);
        product.setName("Dummy");
        product.setQuantity(100);
        product.setSaleAmount(-10);

        //when:
        Set<ConstraintViolation<Product>> violations
                = validator.validate(product);

        //then:
        assertEquals(violations.size(), 1);

        ConstraintViolation<Product> violation
                = violations.iterator().next();
        assertEquals("must be greater than or equal to 0",
                violation.getMessage());
        assertEquals("saleAmount", violation.getPropertyPath().toString());
    }

    @Test
    public void shouldDetectAllInvalid() {
        //given too short name:
        Product product = ProductFacotry.getProduct();

        product.setId(-1);
        product.setName("");
        product.setQuantity(-100);
        product.setSaleAmount(-10);

        //when:
        Set<ConstraintViolation<Product>> violations
                = validator.validate(product);

        //then:
        assertEquals(violations.size(), 4);
    }
}