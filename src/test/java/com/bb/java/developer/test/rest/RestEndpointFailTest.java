package com.bb.java.developer.test.rest;

import com.bb.java.developer.test.BBJDTestApplication;
import com.bb.java.developer.test.config.H2TestConfig;
import com.bb.java.developer.test.error.handling.ApiErrorResponse;
import org.json.JSONException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.LinkedHashMap;
import java.util.List;


@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = {H2TestConfig.class, BBJDTestApplication.class},
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RestEndpointFailTest {

    @LocalServerPort
    private int port;

    TestRestTemplate testRestTemplate;
    RestTemplate restTemplate;
    HttpHeaders headers;

    @Before
    public void setUp(){
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        this.restTemplate = new RestTemplate(requestFactory);
        this.testRestTemplate = new TestRestTemplate();
        this.headers = new HttpHeaders();
        this.headers.setContentType(MediaType.APPLICATION_JSON);
    }

    @Test
    public void testGetOneStringId() throws JSONException {

        ResponseEntity<ApiErrorResponse> response = testRestTemplate.getForEntity(
                createURLWithPort("/products/wrongFormat"), ApiErrorResponse.class);

        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        Assert.assertEquals(response.getHeaders().getContentType(), MediaType.APPLICATION_JSON_UTF8);

    }

    @Test
    public void testGetOneNegativeId() throws JSONException {

        ResponseEntity<ApiErrorResponse> response = testRestTemplate.getForEntity(
                createURLWithPort("/products/-6"), ApiErrorResponse.class);

        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        Assert.assertEquals(response.getHeaders().getContentType(), MediaType.APPLICATION_JSON_UTF8);

    }

    @Test
    public void testGetOneFailed() throws JSONException {

        ResponseEntity<ApiErrorResponse> response = testRestTemplate.getForEntity(
                createURLWithPort("/products/1000"), ApiErrorResponse.class);

        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        Assert.assertEquals(response.getHeaders().getContentType(), MediaType.APPLICATION_JSON_UTF8);

    }

    @Test
    public void testPostMalformed() throws JSONException {

        HttpEntity<String> entity = new HttpEntity<String>("{}", headers);

        ResponseEntity<ApiErrorResponse> response = testRestTemplate.postForEntity(
                createURLWithPort("/products"), entity,ApiErrorResponse.class);

        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        Assert.assertEquals(response.getHeaders().getContentType(), MediaType.APPLICATION_JSON_UTF8);

    }

    @Test
    public void testPutInvalid() throws JSONException {

        HttpEntity<String> entity = new HttpEntity<>("{\"name\":\"testPutInvalid\",\"quantity\":-100,\"sale_amount\":10}", headers);

        ResponseEntity<ApiErrorResponse> response = testRestTemplate.exchange(
                createURLWithPort("/products/1"),HttpMethod.PUT, entity, ApiErrorResponse.class);

        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        Assert.assertEquals(response.getHeaders().getContentType(), MediaType.APPLICATION_JSON_UTF8);

    }

    @Test
    public void testPutMalformed() throws JSONException {

        HttpEntity<String> entity = new HttpEntity<>("{\"name\":\"testPutMalformed\",\"quantity\":\"sdf10\",\"sale_amount\":10}", headers);

        ResponseEntity<ApiErrorResponse> response = testRestTemplate.exchange(
                createURLWithPort("/products/1"),HttpMethod.PUT, entity, ApiErrorResponse.class);

        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        Assert.assertEquals(response.getHeaders().getContentType(), MediaType.APPLICATION_JSON_UTF8);

    }

    @Test
    public void testPatchBad() throws JSONException {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>("{\"name\":\"testPatch\",\"quantity\":-200}", headers);

        ResponseEntity<ApiErrorResponse> response = testRestTemplate.exchange(
                createURLWithPort("/products/1"),HttpMethod.PATCH, entity, ApiErrorResponse.class);

        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        Assert.assertEquals(response.getHeaders().getContentType(), MediaType.APPLICATION_JSON_UTF8);

    }

    @Test
    public void testDeleteInvalid() throws JSONException {

        HttpEntity<String> entity = new HttpEntity<String>(null, headers);


        ResponseEntity<ApiErrorResponse> response = testRestTemplate.exchange(
                createURLWithPort("/products/-145"),HttpMethod.DELETE, entity, ApiErrorResponse.class);


        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        Assert.assertEquals(response.getHeaders().getContentType(), MediaType.APPLICATION_JSON_UTF8);

    }


    @Test
    public void testDeleteNotFound() throws JSONException {

        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<ApiErrorResponse> response = testRestTemplate.exchange(
                createURLWithPort("/products/45435"),HttpMethod.DELETE, entity, ApiErrorResponse.class);

        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        Assert.assertEquals(response.getHeaders().getContentType(), MediaType.APPLICATION_JSON_UTF8);

    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + "/api/v1" + uri;
    }
}