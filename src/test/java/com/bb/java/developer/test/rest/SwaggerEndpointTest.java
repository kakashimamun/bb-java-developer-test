package com.bb.java.developer.test.rest;

import com.bb.java.developer.test.BBJDTestApplication;
import com.bb.java.developer.test.config.H2TestConfig;
import com.bb.java.developer.test.config.SwaggerConfig;
import com.bb.java.developer.test.config.ValidationConfig;
import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = {H2TestConfig.class,BBJDTestApplication.class, SwaggerConfig.class, ValidationConfig.class},
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SwaggerEndpointTest {

    @LocalServerPort
    private int port;

    TestRestTemplate restTemplate = new TestRestTemplate();

    HttpHeaders headers = new HttpHeaders();

    @Test
    public void testSwaggerApi() throws JSONException {

        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/v2/api-docs"),
                HttpMethod.GET, entity, String.class);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals(response.getHeaders().getContentType(),MediaType.APPLICATION_JSON_UTF8);
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + "/api/v1"+uri;
    }
}