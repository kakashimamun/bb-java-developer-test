package com.bb.java.developer.test.validation;

import com.bb.java.developer.test.entities.ProductVO;
import org.hibernate.validator.HibernateValidator;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
public class ProductVoValidationTest {

    private static Validator validator;

    private static LocalValidatorFactoryBean localValidatorFactory;

    @Before
    public void setup() {
        localValidatorFactory = new LocalValidatorFactoryBean();
        localValidatorFactory.setProviderClass(HibernateValidator.class);
        localValidatorFactory.afterPropertiesSet();

        validator = localValidatorFactory.getValidator();
    }

    @AfterClass
    public static void close() {
        localValidatorFactory.close();
    }

    @Test
    public void shouldHaveNoViolations() {
        //given:
        ProductVO vo = new ProductVO();

        vo.setName("Dummy");
        vo.setQuantity(100);
        vo.setSaleAmount(10);

        //when:
        Set<ConstraintViolation<ProductVO>> violations
                = validator.validate(vo);

        //then:
        assertTrue(violations.isEmpty());
    }

    @Test
    public void shouldDetectInvalidName() {
        //given too short name:
        ProductVO vo = new ProductVO();

        vo.setName("");
        vo.setQuantity(100);
        vo.setSaleAmount(10);

        //when:
        Set<ConstraintViolation<ProductVO>> violations
                = validator.validate(vo);

        //then:
        assertEquals(violations.size(), 1);

        ConstraintViolation<ProductVO> violation
                = violations.iterator().next();
        assertEquals("may not be empty",
                violation.getMessage());
        assertEquals("name", violation.getPropertyPath().toString());
    }

    @Test
    public void shouldDetectInvalidQuantity() {
        //given too short name:
        ProductVO vo = new ProductVO();

        vo.setName("Dummy");
        vo.setQuantity(-100);
        vo.setSaleAmount(10);

        //when:
        Set<ConstraintViolation<ProductVO>> violations
                = validator.validate(vo);

        //then:
        assertEquals(violations.size(), 1);

        ConstraintViolation<ProductVO> violation
                = violations.iterator().next();
        assertEquals("must be greater than or equal to 0",
                violation.getMessage());
        assertEquals("quantity", violation.getPropertyPath().toString());
    }

    @Test
    public void shouldDetectInvalidSaleAmount() {
        //given too short name:
        ProductVO vo = new ProductVO();

        vo.setName("Dummy");
        vo.setQuantity(100);
        vo.setSaleAmount(-10);

        //when:
        Set<ConstraintViolation<ProductVO>> violations
                = validator.validate(vo);

        //then:
        assertEquals(violations.size(), 1);

        ConstraintViolation<ProductVO> violation
                = violations.iterator().next();
        assertEquals("must be greater than or equal to 0",
                violation.getMessage());
        assertEquals("saleAmount", violation.getPropertyPath().toString());
    }

    @Test
    public void shouldDetectAllInvalid() {
        //given too short name:
        ProductVO vo = new ProductVO();

        vo.setName("");
        vo.setQuantity(-100);
        vo.setSaleAmount(-10);

        //when:
        Set<ConstraintViolation<ProductVO>> violations
                = validator.validate(vo);

        //then:
        assertEquals(violations.size(), 3);

    }
}