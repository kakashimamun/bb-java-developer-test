package com.bb.java.developer.test.factory;

import com.bb.java.developer.test.entities.ProductVO;
import com.bb.java.developer.test.models.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;


@RunWith(SpringRunner.class)
public class ProductFacotryTest {

    @Test
    public void getProduct() {
        Assert.isInstanceOf(Product.class,ProductFacotry.getProduct());
        Assert.notNull(ProductFacotry.getProduct(),"product is null");
    }

    @Test
    public void getProductWithVo() {
        ProductVO vo = new ProductVO();

        vo.setName("Dummy");
        vo.setQuantity(100);
        vo.setSaleAmount(10);

        Product product = ProductFacotry.getProduct(vo);

        Assert.isInstanceOf(Product.class,product);
        Assert.notNull(product,"instance is null");
        Assert.isTrue(product.getId()==0,"");
        Assert.isTrue(product.getName().equals("Dummy"),"");
        Assert.isTrue(product.getQuantity()==100,"");
        Assert.isTrue(product.getSaleAmount()==10,"");

    }
}