package com.bb.java.developer.test.rest;

import com.bb.java.developer.test.BBJDTestApplication;
import com.bb.java.developer.test.config.H2TestConfig;
import com.bb.java.developer.test.config.ValidationConfig;
import com.bb.java.developer.test.entities.ApiResponseEntity;
import com.bb.java.developer.test.entities.ProductIdWrapper;
import org.json.JSONException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.LinkedHashMap;
import java.util.List;


@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = {H2TestConfig.class, BBJDTestApplication.class,ValidationConfig.class},
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RestEndpointSuccessTest {

    @LocalServerPort
    private int port;

    TestRestTemplate testRestTemplate;
    RestTemplate restTemplate;
    HttpHeaders headers;

    @Before
    public void setUp(){
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        this.restTemplate = new RestTemplate(requestFactory);
        this.testRestTemplate = new TestRestTemplate();
        this.headers = new HttpHeaders();
        this.headers.setContentType(MediaType.APPLICATION_JSON);
    }

    @Test
    public void testGet() throws JSONException {

        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        Class<ApiResponseEntity<List<ProductIdWrapper>>> responseEntityClass = (Class) ApiResponseEntity.class;
        ResponseEntity<ApiResponseEntity<List<ProductIdWrapper>>> response = testRestTemplate.getForEntity(
                createURLWithPort("/products"), responseEntityClass);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals(response.getHeaders().getContentType(), MediaType.APPLICATION_JSON_UTF8);

        Assert.assertTrue(response.getBody().getProducts().size() != 0);
    }

    @Test
    public void testGetPageable() throws JSONException {

        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        Class<ApiResponseEntity<List<ProductIdWrapper>>> responseEntityClass = (Class) ApiResponseEntity.class;
        ResponseEntity<ApiResponseEntity<List<ProductIdWrapper>>> response = testRestTemplate.getForEntity(
                createURLWithPort("/products/pageable?page=0&size=5"), responseEntityClass);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals(response.getHeaders().getContentType(), MediaType.APPLICATION_JSON_UTF8);

        Assert.assertTrue(response.getBody().getProducts().size() == 5);
    }

    @Test
    public void testGetOne() throws JSONException {

        Class<ApiResponseEntity<List<LinkedHashMap>>> responseEntityClass = (Class) ApiResponseEntity.class;
        ResponseEntity<ApiResponseEntity<List<LinkedHashMap>>> response = testRestTemplate.getForEntity(
                createURLWithPort("/products/2"), responseEntityClass);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals(response.getHeaders().getContentType(), MediaType.APPLICATION_JSON_UTF8);

        Assert.assertTrue(response.getBody().getProducts().size() == 1);
        Assert.assertEquals(response.getBody().getProducts().get(0).get("id"), 2);
        Assert.assertEquals(response.getBody().getProducts().get(0).get("name"), "Maserati");
    }

    @Test
    public void testPost() throws JSONException {

        HttpEntity<String> entity = new HttpEntity<String>("[{\"name\":\"testPost1\",\"quantity\":100,\"sale_amount\":10},{\"name\":\"testPost2\",\"quantity\":100,\"sale_amount\":10},{\"name\":\"testPost3\",\"quantity\":100,\"sale_amount\":10}]", headers);

        Class<ApiResponseEntity<List<LinkedHashMap>>> responseEntityClass = (Class) ApiResponseEntity.class;
        ResponseEntity<ApiResponseEntity<List<LinkedHashMap>>> response = testRestTemplate.postForEntity(
                createURLWithPort("/products"), (Object) entity, responseEntityClass);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals(response.getHeaders().getContentType(), MediaType.APPLICATION_JSON_UTF8);

        Assert.assertTrue(response.getBody().getProducts().size() == 3);
        Assert.assertEquals(response.getBody().getProducts().get(0).get("name"), "testPost1");
        Assert.assertEquals(response.getBody().getProducts().get(0).get("quantity"), 100);
        Assert.assertEquals(response.getBody().getProducts().get(0).get("sale_amount"), 10);
        Assert.assertEquals(response.getBody().getProducts().get(1).get("name"), "testPost2");
        Assert.assertEquals(response.getBody().getProducts().get(2).get("name"), "testPost3");
    }

    @Test
    public void testPut() throws JSONException {

        HttpEntity<String> entity = new HttpEntity<>("{\"name\":\"testPut\",\"quantity\":100,\"sale_amount\":10}", headers);

        Class<ApiResponseEntity<List<LinkedHashMap>>> responseEntityClass = (Class) ApiResponseEntity.class;
        ResponseEntity<ApiResponseEntity<List<LinkedHashMap>>> response = testRestTemplate.exchange(
                createURLWithPort("/products/1"),HttpMethod.PUT, entity, responseEntityClass);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals(response.getHeaders().getContentType(), MediaType.APPLICATION_JSON_UTF8);

        Assert.assertTrue(response.getBody().getProducts().size() == 1);
        Assert.assertEquals(response.getBody().getProducts().get(0).get("name"), "testPut");
        Assert.assertEquals(response.getBody().getProducts().get(0).get("quantity"), 100);
        Assert.assertEquals(response.getBody().getProducts().get(0).get("sale_amount"), 10);
    }


    @Test
    public void testPatch() throws JSONException {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>("{\"name\":\"testPatch\",\"quantity\":200}", headers);

        Class<ApiResponseEntity<List<LinkedHashMap>>> responseEntityClass = (Class) ApiResponseEntity.class;
        ResponseEntity<ApiResponseEntity<List<LinkedHashMap>>> response = restTemplate.exchange(
                createURLWithPort("/products/1"),HttpMethod.PATCH, entity, responseEntityClass);



        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals(response.getHeaders().getContentType(), MediaType.APPLICATION_JSON_UTF8);

        Assert.assertTrue(response.getBody().getProducts().size() == 1);
        Assert.assertEquals(response.getBody().getProducts().get(0).get("name"), "testPatch");
        Assert.assertEquals(response.getBody().getProducts().get(0).get("quantity"), 200);
        Assert.assertEquals(response.getBody().getProducts().get(0).get("sale_amount"), 10);
    }

    @Test
    public void testDelete() throws JSONException {

        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        Class<ApiResponseEntity<List<LinkedHashMap>>> responseEntityClass = (Class) ApiResponseEntity.class;
        ResponseEntity<ApiResponseEntity<List<LinkedHashMap>>> response = testRestTemplate.exchange(
                createURLWithPort("/products/1"),HttpMethod.DELETE, entity, responseEntityClass);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals(response.getHeaders().getContentType(), MediaType.APPLICATION_JSON_UTF8);

        response = testRestTemplate.getForEntity(
                createURLWithPort("/products/1"), responseEntityClass);

        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        Assert.assertEquals(response.getHeaders().getContentType(), MediaType.APPLICATION_JSON_UTF8);

    }




    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + "/api/v1" + uri;
    }
}