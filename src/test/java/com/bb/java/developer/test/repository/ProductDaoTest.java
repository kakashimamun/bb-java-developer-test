package com.bb.java.developer.test.repository;

import com.bb.java.developer.test.config.H2TestConfig;
import com.bb.java.developer.test.factory.ProductFacotry;
import com.bb.java.developer.test.models.Product;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest(showSql = true)
@EntityScan(basePackages = "com.bb.java.developer.test")
@Import({H2TestConfig.class})
public class ProductDaoTest {

    @Autowired
    private IProductRepository productDao;

    @Test
    public void testSave() {
        Product product = ProductFacotry.getProduct();
        product.setId(100);
        product.setName("Dummy");
        product.setQuantity(1000);
        product.setSaleAmount(30);
        product = productDao.save(product);

        Product saved = productDao.findOne(product.getId());
        Assert.assertTrue(product.equals(saved));
    }

    @Test
    public void testFindOne() {
        Product product = productDao.findOne(2L);

        Assert.assertEquals(product.getId(), 2);
        Assert.assertEquals(product.getName(), "Maserati");
        Assert.assertEquals(product.getQuantity(), 747);
        Assert.assertEquals(product.getSaleAmount(), 104);
    }

    @Test
    public void testGetAllIds() {
        List<Long> ids = productDao.getAllId();

        Assert.assertTrue(ids.size() != 0);
    }

    @Test
    public void testGetAllIdsPageable() {
        Pageable pageable = new PageRequest(0, 5);
        List<Long> ids = productDao.getAllId(pageable);
        Assert.assertTrue(ids.size() != 0);
        Assert.assertTrue(ids.size() == 5);
        Assert.assertTrue(ids.get(0) == 1);
    }

    @Test
    public void testDelete() {
        long id = 5;
        productDao.delete(id);
        Product p = productDao.findOne(id);
        Assert.assertNull(p);
    }


}